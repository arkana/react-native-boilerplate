export default {
    primary: "#C01820",
    white: "#FFFFFF",
    black: "#000000",
    black25: "#cccccc",
    black50: "#7d7d7d",
    
    gray: "#CCCCCC",
    danger: "#e74c3c",
    success: "#2ecc71",

    Neutral80: "#A5A5A5",
    Neutral20: "#F6F6F6",
    Neutral10: "#6C6C6C",

    themeGray: "#e9e9e9",
}