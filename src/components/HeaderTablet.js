import React, { component, useCallback, useEffect, useState } from "react"
import { Text, View, TouchableOpacity, Dimensions, SafeAreaView, Alert } from "react-native"
import color from "../utils/color";
import Feather from 'react-native-vector-icons/Feather';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { fonts } from "../utils/fonts";
import { useDispatch, useSelector } from "react-redux";
import SimpleModal from "./SimpleModal";
import { setLockScreen, setUser } from "../store/actions";
import Button from "./Button";
import StyleUtils from "../utils/StyleUtils";
import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { HttpRequest } from "../utils/http";
import Toast from "./Toast";

const SCREEN_WIDTH = Dimensions.get("window").width

export default function HeaderTablet(props) {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user);

    const [isLoading, setIsLoading] = useState(false);
    const [showLogoutModal, setShowLogoutModal] = useState(false);
    const [showPinInputModal, setShowPinInputModal] = useState(false);
    const [value, setValue] = useState("");
    const [codeProps, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    });

    useEffect(() => {
        console.log("User", user);
    }, [user]);

    const gantiShift = useCallback(() => {
        setIsLoading(true);
        HttpRequest.changeShift({ pin: value }).then((res) => {
            setIsLoading(false);

            dispatch(setUser(res.data));
            Toast.showSuccess("Anda berhasil login");

            setShowLogoutModal(false);
            setShowPinInputModal(false);
        }).catch((err) => {
            console.log(err);
            Toast.showError(err.response.data.message);
            setIsLoading(false);
        });
    }, [value]);

    return (
        <>
            <SafeAreaView style={{ backgroundColor: color.black }} />
            <View style={styles.container}>
                {/* <View style={styles.absolute}>
                    <Text style={styles.txtHeader}>LAUNDRO</Text>
                </View> */}
                <TouchableOpacity style={styles.menuLeft} onPress={props.iconLeft} activeOpacity={0.8}>
                    <Feather name="menu" size={24} color={color.white} />
                </TouchableOpacity>
                <View style={{ flex: 1 }}>

                </View>
                <TouchableOpacity style={styles.menuRight} onPress={() => {
                    setShowLogoutModal(true);
                }}>
                    <Text style={styles.txtProfile}>{user?.user?.name}</Text>

                    <EvilIcons name="user" size={34} color={color.white} />
                </TouchableOpacity>
            </View>

            <SimpleModal
                visible={showLogoutModal}
                onRequestClose={() => {
                    setShowLogoutModal(false);
                }}
                modal={{ backgroundColor: "rgba(0, 0, 0, 0.7)" }}
                content={{ borderColor: "transparent", width: 400, backgroundColor: "transparent" }}
                contentStyle={styles.modalContent}>
                <Text style={StyleUtils.titleStyle}>Anda ingin apa ?</Text>

                <Button
                    theme='danger'
                    onPress={() => {
                        Alert.alert(
                            'Informasi',
                            'Anda yakin ingin logout ?',
                            [
                                { text: 'No', onPress: () => { }, style: 'cancel' },
                                {
                                    text: 'Yes', onPress: () => {
                                        setShowLogoutModal(false);

                                        dispatch(setUser(null));
                                    }
                                },
                            ],
                            { cancelable: false }
                        );
                    }}>
                    Logout
                </Button>
            </SimpleModal>

            <SimpleModal
                visible={showPinInputModal}
                onRequestClose={() => {
                    setShowPinInputModal(false);
                }}
                modal={{ backgroundColor: "rgba(0, 0, 0, 0.7)" }}
                content={{ borderColor: "transparent", width: 400, backgroundColor: "transparent" }}
                contentStyle={styles.modalContent}>
                <Text style={StyleUtils.titleStyle}>Masukkan PIN Anda</Text>

                <CodeField
                    {...codeProps}
                    // Use `caretHidden={false}` when users can't paste a text value, because context menu doesn't appear
                    value={value}
                    onChangeText={setValue}
                    cellCount={6}
                    rootStyle={styles.codeFieldRoot}
                    keyboardType="number-pad"
                    textContentType="oneTimeCode"
                    renderCell={({ index, symbol, isFocused }) => {
                        let textChild = null;

                        if (symbol) {
                            textChild = '•';
                        } else if (isFocused) {
                            textChild = <Cursor />;
                        }

                        return (
                            <Text
                                key={index}
                                style={[styles.cell, isFocused && styles.focusCell]}
                                onLayout={getCellOnLayoutHandler(index)}>
                                {textChild}
                            </Text>
                        );
                    }}
                />

                <Button
                    style={{ marginBottom: 15 }}
                    onPress={() => {
                        gantiShift();
                    }}>
                    Login
                </Button>

                <Button
                    theme='danger'
                    onPress={() => {
                        setShowPinInputModal(false);
                        setShowLogoutModal(true);
                    }}>
                    Batal
                </Button>
            </SimpleModal>
        </>
    )
}

const styles = {
    container: {
        flexDirection: "row",
        alignItems: "center",
        width: SCREEN_WIDTH,
        backgroundColor: color.primary,
        height: 60,
    },
    absolute: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: 'red',
    },
    menuLeft: {
        marginLeft: 20,
    },
    menuRight: {
        marginRight: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    txtHeader: {
        alignItems: 'center',
        alignSelf: "center",
        fontSize: 36,
        // fontWeight: "bold",
        fontFamily: fonts.montserratBold,
        color: color.primary,
    },
    userCircle: {
        backgroundColor: color.black,
        height: 42,
        width: 42,
        borderRadius: 21,
        justifyContent: 'center',
        alignItems: 'center',
    },
    txtProfile: {
        marginRight: 5,
        fontSize: 16,
        color: color.white,
    },

    codeFieldRoot: {
        marginVertical: 15,
    },
    cell: {
        width: 40,
        height: 40,
        lineHeight: 38,
        fontSize: 24,
        borderWidth: 1,
        borderColor: color.gray,
        borderRadius: 8,
        textAlign: 'center',
    },
    focusCell: {
        borderColor: '#000',
    },
}