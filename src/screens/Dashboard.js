import React from 'react';
import { View, Text, Dimensions } from 'react-native';
import HeaderTablet from '../components/HeaderTablet';
import app from '../config/app';
import { fonts } from '../utils/fonts';
import color from '../utils/color';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Button from '../components/Button';

const SCREEN_HEIGHT = Dimensions.get("window").height;

export default function Dashboard(props) {
    return (
        <View style={styles.container}>
            <HeaderTablet
                textHeader={app.NAME}
                textStyleHeader={styles.txtHeader}
                textProfile={"Admin"}
                iconLeft={() => props.navigation.openDrawer()}
            />

            <View style={{ padding: 20 }}>
                <Text>Ini adalah halaman dashboard</Text>

                <Button onPress={() => {
                    props.navigation.navigate('UpdateHarga', { id: 1 });
                }}>Update Harga</Button>
            </View>
        </View>
    );
}

const styles = {
    container: {
        backgroundColor: color.white,
        flex: 1,
    },
    txtHeader: {
        fontFamily: fonts.montserratBold,
        fontSize: 16,
        marginBottom: 16,
        color: color.black,
    },
}